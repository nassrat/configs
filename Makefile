all: usage
.PHONY: vim myzsh hgrc

usage:
	@echo
	@echo "targets:"
	@echo 
	@echo  "   vim      : hookup vim configs"
	@echo  "   myzsh    : hookup custom oh-my-zsh"
	@echo  "   hgrc     : hookup hgrc configs"
	@echo

vim:
	ln -s $(CURDIR)/.vimrc ~/.vimrc
	ln -s $(CURDIR)/.vim ~/.vim
	vim +BundleInstall +qall

myzsh:
	@test -d ~/.oh-my-zsh || (echo 'INSTALL ZSH USING THIS COMMAND -> sh -c "$$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"' && false)
	mv ~/.zshrc ~/.zshrc.old || true
	ln -s $(CURDIR)/.zshrc ~/.zshrc
	ln -s $(CURDIR)/.oh-my-zsh-custom ~/.oh-my-zsh-custom

hgrc:
	ln -s $(CURDIR)/hgrc/.hgrc ~/.hgrc
	ln -s $(CURDIR)/hgrc/.hgext ~/.hgext
	ln -s $(CURDIR)/hgrc/.hgignore ~/.hgignore
