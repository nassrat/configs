Config extension for mercurial
******************************

This extension provides command-line access to hg configuration values stored
in hgrc files. You can use this extension to view and change configuration
values, show which configuration files are used by hg, and edit any of these
files from the command-line.

Three commands are provided by this extension:

- hg showconfigs
- hg editconfig
- hg config