" Vundle {{{
set nocompatible
filetype off

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" let Vundle manage Vundle
" required!
Bundle 'gmarik/vundle'

" The bundles you install will be listed here
"Bundle 'Lokaltog/powerline', {'rtp': 'powerline/bindings/vim/'}
Bundle 'scrooloose/nerdtree'
" Bundle 'scrooloose/nerdcommenter'
Bundle 'scrooloose/syntastic'
Bundle 'kien/rainbow_parentheses.vim'
Bundle 'tpope/vim-unimpaired'
" Bundle 'tpope/vim-surround'
" Bundle 'tpope/vim-speeddating' " for fast date conversion
Bundle 'vim-scripts/YankRing.vim'
Bundle 'mileszs/ack.vim'
" Bundle 'Shougo/neocomplcache' " do not really auto comp
Bundle 'nathanaelkane/vim-indent-guides'
" Bundle 'vim-scripts/indent-motion'
" Bundle 'Raimondi/delimitMate'
Bundle 'klen/python-mode'
Bundle 'briancollins/vim-jst'
Bundle 'derekwyatt/vim-scala'
" Bundle 'csv.vim'
" Bundle 'altercation/vim-colors-solarized'

" The rest of your config follows here
" }}}

" Number config {{{
set relativenumber
" }}}

" Tabs config {{{
set shiftwidth=4
set autoindent
set smarttab
set tabstop=4
set expandtab
" }}}

" Searching Params {{{
set ignorecase
set smartcase
set showmatch
set hlsearch
set incsearch
" }}}

" Modeline Related {{{
set modeline
set modelines=5
set showmode
set showcmd
" }}}

" Text width {{{
"set textwidth=79
" :au BufNewFile,BufRead * exec 'match Folded /\%>' . (&tw) .     'v.\+/' " Full Line
" :au BufNewFile,BufRead * exec 'match Folded /\%' . (&tw + 1) .     'v/'
" match ErrorMsg '\%>80v.\+' " Full Line
" }}}

" Maps (All Maps can only come after this point) {{{
" maps config
let mapleader = ","

" Map ,a to clean extra endline tabs/spaces
nnoremap <silent> <leader>a :%s,\s\+$,,<CR>

" Window moving maps
map <C-J> <C-W>j<C-W>_
map <C-K> <C-W>k<C-W>_
set wmh=0

" Turn off hlsearch when it gets annoying (\n)
nmap <silent> <leader>n :silent :nohlsearch<CR>

" spelling <C-n> & <C-p> to auto-comp
set complete-=k complete+=k
set spellfile=~/.vim/spellfile.add
map <F7> :setlocal spell! spelllang=en_gb<CR>

" Turn the mouse on
let g:mouseOn = 0
map <silent> <F8>
    \ :if g:mouseOn == 0 <Bar>
    \   set mouse=a <Bar>
    \   let g:mouseOn = 1 <Bar>
    \ else <Bar>
    \   set mouse= <Bar>
    \   let g:mouseOn = 0 <Bar>
    \ endif <CR>

" Toggle word wrap
map <F6> <Esc>:set wrap!<CR>

" Highlight Chars >textwidth || >79
let g:long_line_match = -1
highlight rightMargin term=reverse cterm=reverse
nnoremap <silent> <leader>l :call HiLongText(0)<CR>
nnoremap <silent> <leader>L :call HiLongText(1)<CR>

function! HiLongText(full_line)
    let extra = ''
    if a:full_line == 1
        let extra = '\+'
    endif
    if g:long_line_match == a:full_line
        match
        let g:long_line_match = -1
    elseif &textwidth > 0
        exec 'match rightMargin /\%'.(&tw + 1).'v.'.(extra).'/'
        let g:long_line_match = a:full_line
    else
        exec 'match rightMargin /\%80v.'.(&extra).'/'
        let g:long_line_match = a:full_line
    endif
endfunction

" Toggle word wrap
map <F5> <Esc>:set paste!<CR>

" Force save a root writeable file
cmap w!! w !sudo tee %

" }}}

" Syntax {{{
syntax enable
" File-type highlighting and configuration.
" Run :filetype (without args) to see what you may have
" to turn on yourself, or just set them all to be sure.
syntax on
filetype on
filetype plugin on
filetype indent on
" Filetypes
so ~/.vim/filetypes.vim
" }}}

" Some useful abbreviations to common mistyped commands {{{
cab W w | cab Q q | cab Wq wq | cab wQ wq | cab WQ wq
" }}}

" Remem Cursor positions {{{
set viminfo='10,\"100,:20,%,n~/.viminfo
au BufReadPost * if line("'\"") > 0|if line("'\"") <= line("$")|exe("norm '\"")|else|exe "norm $"|endif|endif
" }}}

" Set netrw to sync current dir with vim {{{
let g:netrw_keepdir=0
" }}}

" Window and Term configs {{{
" No beeps
set visualbell
" Window Title
set title
" Start scrolling 3 lines away from border
set scrolloff=3
" Intuitive backspacing in insert mode
set backspace=indent,eol,start
" Set shorter messages
set shortmess=atI
" Set longer history
set history=1000
" Status Bar
set laststatus=2
set statusline=%F%m%r%h%w\ %y\ [%{&ff}]\ %{FileTime()}%=[%l,%v][%p%%]\ [%L]
set rulerformat=%15(%c%V\ %p%%%)

" Defining Status Bar Funcs
fu! FileTime()
 let ext=tolower(expand("%:e"))
 let fname=tolower(expand('%<'))
 let filename=fname . '.' . ext
 let msg=""
 let msg=msg." ".strftime("(Modified %b,%d %y %H:%M:%S)",getftime(filename))
 return msg
endf

fu! CurTime()
 let ftime=""
 let ftime=ftime." ".strftime("%b,%d %y %H:%M:%S")
 return ftime
endf
" }}}

" Plugins setup {{{
" Tabs
" let g:SuperTabDefaultCompletionType = "<C-X><C-O>"
" let g:SuperTabMappingForward = '<c-space>'
" let g:SuperTabMappingBackward = '<s-c-space>'
let g:SuperTabMappingForward = '<nul>'
let g:SuperTabMappingBackward = '<s-nul>'

" " Trac Vim
" let g:tracServerList = {}
" let g:tracServerList['twecan'] = 'http://hatem:hal3aseeto@twecan.homeip.net/trac/twecan-project/login/xmlrpc'

" Plugins source
"so ~/.vim/plugins/supertab.vim
"so ~/.vim/plugins/xml.vim

" vim-indent-guides
let g:indent_guides_start_level = 2
let g:indent_guides_guide_size = 1
let g:indent_guides_auto_colors = 0
let g:indent_guides_enable_on_vim_startup = 1
autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  ctermbg=233
autocmd VimEnter,Colorscheme * :hi IndentGuidesEven ctermbg=237

" Nerd Commenter
"map <leader>C <plug>NERDCommenterComment
"map <leader>U <plug>NERDCommenterUncomment

" Nerd Tree
"" starts nerdtree by default
"autocmd VimEnter * NERDTree
"" folder mirroring TODO: figure what this is
"autocmd BufEnter * NERDTreeMirror
let NERDTreeShowHidden=1
let g:NERDTreeDirArrows=0
noremap <leader>q :NERDTreeToggle<CR>

" Python Mode
let g:pymode_rope_complete_on_dot = 0
" Disable python folding
let g:pymode_folding = 0
" Disable pylint checking every save
let g:pymode_lint_on_write = 0
map <leader>x :PymodeLint<CR>
map <leader>X :sign unplace *<CR>
" Auto open cwindow if errors were found
let g:pymode_lint_cwindow = 1
" author uses "pyflakes" with flask, and "pylint" with django
"let g:pymode_lint_checkers = ['pyflakes','pep8','mccabe']
let g:pymode_lint_checkers = ['pyflakes']
" " Don't place error signs
" let g:pymode_lint_signs = 0
" let g:pymode_options = 0
let g:pymode_rope = 0
let g:pymode_virtualenv = 1

" Rainbow parenthesis
au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces

" Syntastic
let g:syntastic_javascript_checkers = ['jshint']
let g:syntastic_auto_loc_list = 1
" let g:syntastic_mode_map = { 'mode': 'passive', 'active_filetypes': ['javascript'] }
let g:syntastic_mode_map = { 'mode': 'passive' }
map <leader>s :SyntasticCheck<CR>

" }}}

" Plugins setup {{{
if $VIM_CRONTAB == "true"
    set nobackup
    set nowritebackup
endif
" }}}

" Colors {{{
if &diff
    " set t_Co=256
    colorscheme peaksea
else
    " colorscheme ir_black
    " colorscheme blackbeauty
    " colorscheme danger
    " colorscheme tir_black " has some nice colors, try to pick them up
    " colorscheme 256-jungle-custom
    "colorscheme delek
    "colorscheme default
    color desert
endif
" for dark displays
set background=dark
" For visible Spel Local highlight
hi SpellLocal ctermbg=darkred
" helper commands, this shows the color matching name
" :echo map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")')
" this shows the color under cursor
" :echo synIDattr(synIDtrans(synID(line("."), col("."), 1)), "fg")
" :echo synIDattr(synIDtrans(synID(line("."), col("."), 1)), "bg")
" :hi ... will show all the colors
" put =execute('hi') ... to dump the colors to the current screen
hi level10c ctermfg=green
" }}}
