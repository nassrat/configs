if [ $UID -eq 0 ]; then NCOLOR="red"; else NCOLOR="green"; fi
local return_code="%(?..%{$fg[red]%}%? %{$fg_bold[blue]%})"

PROMPT=$'%{$fg_bold[$NCOLOR]%}%n@%m %{$fg[blue]%}%D{[%I:%M:%S]} %{$reset_color%}%{$fg[white]%}[%~]%{$reset_color%} $(git_prompt_info)$(hg_prompt_info)\
%{$fg[blue]%}->%{$fg_bold[blue]%} ${return_code}%{$fg_bold[$NCOLOR]%}%#%{$reset_color%} '

ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg[green]%}[git:"
ZSH_THEME_GIT_PROMPT_SUFFIX="]%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY=" %{$fg[red]%}*%{$fg[green]%}"
ZSH_THEME_GIT_PROMPT_CLEAN=""

ZSH_THEME_HG_PROMPT_PREFIX="%{$fg[green]%}[hg:"
ZSH_THEME_HG_PROMPT_SUFFIX="]%{$reset_color%}"
