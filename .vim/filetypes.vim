augroup filetype
        au!
        au! BufRead,BufNewFile *.phc    set filetype=php
        au! BufRead,BufNewFile *.mine   set filetype=mine
        au! BufRead,BufNewFile *.cs     set filetype=html
        au! BufRead,BufNewFile *.cst    set filetype=html
        au! BufRead,BufNewFile *.vue    set filetype=html
        au! BufRead,BufNewFile *.json   set filetype=json
        au! BufRead,BufNewFile *.mako   set filetype=mako
        au! BufRead,BufNewFile *.ara    set filetype=arabic
        au! BufRead,BufNewFile *.arabic set filetype=arabic
        au! BufRead,BufNewFile *.moin   set filetype=moin
        au! BufRead,BufNewFile *.wiki   set filetype=moin
        au! BufRead,BufNewFile *.muttrc set filetype=muttrc
        au! BufRead,BufNewFile *.m      set filetype=octave
        au! BufRead,BufNewFile *.sls    set filetype=yaml
augroup END

" augroup vimrc_autocmds
"     autocmd!
"     " highlight characters past column 120
"     autocmd FileType python highlight Excess ctermbg=DarkGrey guibg=Black
"     autocmd FileType python match Excess /\%120v.*/
"     autocmd FileType python set nowrap
" augroup END

let comment = '# '
" Comment/Uncomment for different languages
au FileType haskell,vhdl,ada let comment = '-- '
au FileType sh,make,python   let comment = '# '
au FileType c,cpp,php,java,javascript       let comment = '// '
au FileType tex              let comment = '% '
au FileType vim              let comment = '" '
au FileType mako             let comment = '## '
au FileType xmodmap          let comment = '! '
au FileType octave           let comment = '% '

" " Comment Blocks
noremap <silent> <leader>C :s,^,<C-R>=comment<CR>,<CR>:noh<CR>
noremap <silent> <leader>U :s,^\V<C-R>=comment<CR>,,e<CR>:noh<CR>

au FileType javascript,html,mako,htmldjango,jade,jst       set tw=0 shiftwidth=2 tabstop=2
au FileType yaml,json       set tw=0 shiftwidth=2 tabstop=2
au FileType scala           set shiftwidth=2 tabstop=2
au FileType vim             set tw=0 fdm=marker
au FileType arabic          set arabic

" Completion
autocmd FileType python set omnifunc=pythoncomplete#Complete
autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS
autocmd FileType html set omnifunc=htmlcomplete#CompleteTags
autocmd FileType css set omnifunc=csscomplete#CompleteCSS
autocmd FileType xml set omnifunc=xmlcomplete#CompleteTags
autocmd FileType php set omnifunc=phpcomplete#CompletePHP
autocmd FileType c set omnifunc=ccomplete#CompleteCpp

" Folding
" let g:xml_syntax_folding=1
" au FileType html,mako,xml,html,php setlocal foldmethod=syntax
" au FileType html,mako,xml,html,xhtml,php so ~/.vim/plugins/XMLFold.vim
