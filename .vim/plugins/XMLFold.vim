" Copied from xml.vim, this from the xml-folding 
" As of Feb 2009, its in line 131-189 
syn region   xmlTag 
\ matchgroup=xmlTag start=+<[^ /!?<>"']\@=+ 
\ matchgroup=xmlTag end=+>+ 
\ contained 
\ contains=xmlError,xmlTagName,xmlAttrib,xmlEqual,xmlString,@xmlStartTagHook 

syn match   xmlEndTag 
\ +</[^ /!?<>"']\+>+ 
\ contained 
\ contains=xmlNamespace,xmlAttribPunct,@xmlTagHook 

syn region   xmlRegion 
\ start=+<\z([^ /!?<>"']\+\)+ 
\ skip=+<!--\_.\{-}-->+ 
\ end=+</\z1\_\s\{-}>+ 
\ matchgroup=xmlEndTag end=+/>+ 
\ fold 
\ contains=xmlTag,xmlEndTag,xmlCdata,xmlRegion,xmlComment,xmlEntity,xmlProcessing,@xmlRegionHook,@Spell 
\ keepend 
\ extend 

" http://vim.wikia.com/wiki/Syntax_folding_of_Vim_scripts 
" Folding via syntax is used for this filetype. 
setlocal foldmethod=syntax 
setlocal foldlevel=2 

" VIM's command window ('q:') and the :options window also set filetype=vim. We 
" do not want folding in these enabled by default, though, because some 
" malformed :if, :function, ... commands would fold away everything from the 
" malformed command until the last command. 
if bufname('') =~# '^\%(' . (v:version < 702 ? 'command-line' : '\[Command Line\]') . '\|option-window\)$' 
    " With this, folding can still be enabled easily via any zm, zc, zi, ... 
    " command. 
    setlocal nofoldenable 
else 
    " Fold settings for ordinary windows. 
    setlocal foldcolumn=4 
endif  
